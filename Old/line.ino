#include "DHT.h"
#define DHTPIN D6
#define DHTTYPE DHT11 // DHT 11
#include <TridentTD_LineNotify.h>
//#define DHTTYPE DHT22 // DHT 22 (AM2302), AM2321
//#define DHTTYPE DHT21 // DHT 21 (AM2301)

#define SSID "NO OK" // แก้ ชื่อ ssid ของ wifi 
#define PASSWORD "nook247538" // แก้ รหัสผ่าน wifi
#define LINE_TOKEN "crDfjmD0rPQNnbRXQRS0Aw5f0ztBenkznfkkspZT24b" // แก้ Line Token

DHT dht(DHTPIN, DHTTYPE);
void setup() {
  Serial.begin(9600);
  dht.begin();
  Serial.println();
  Serial.println(LINE.getVersion());
  WiFi.begin(SSID, PASSWORD);
  Serial.printf("WiFi connecting to %s\n", SSID);
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(2000);
  }
  Serial.printf("\nWiFi connected\nIP : ");
  Serial.println(WiFi.localIP());
  LINE.setToken(LINE_TOKEN);
  LINE.notify("เซนเซอร์วัดอุณหภูมิและความชื้น เริ่มทำงานแล้ว");
}

void loop() {
  String val = "";
  float h = dht.readHumidity(); // ความชื้น
  float t = dht.readTemperature(); // อุณหภูมิ

  if (isnan(h) || isnan(t)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }
  val = val + t;
  val = val + "C ";
  val = val + h;
  val = val + "%";
  Serial.println(val);
  LINE.notify(val);
  delay(60000);
}
