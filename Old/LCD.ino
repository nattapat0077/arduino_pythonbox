#include "DHT.h"
#include <LiquidCrystal_I2C.h>
#include "DHT.h"

#define DHTPIN D6
#define DHTTYPE DHT11 // DHT 11
//#define DHTTYPE DHT22 // DHT 22 (AM2302), AM2321
//#define DHTTYPE DHT21 // DHT 21 (AM2301)
DHT dht(DHTPIN, DHTTYPE);

LiquidCrystal_I2C lcd(0x27, 16, 2);
//LiquidCrystal_I2C lcd(0x27, 16, 2); // ถ้าจอไม่ขึ้นให้ลองใช้ตำแหน่ง 0x3F นี้
void setup() {
  dht.begin();
  lcd.begin();

}
void loop() {
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  if (isnan(h) || isnan(t)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }
  lcd.setCursor(0, 0);
  lcd.print("Hum:     ");
  lcd.setCursor(4, 0);
  lcd.print(h);
  lcd.setCursor(9, 0);
  lcd.print("%");
  lcd.setCursor(0, 1);
  lcd.print("Tem:     ");
  lcd.setCursor(4, 1);
  lcd.print(t);
  lcd.setCursor(9, 1);
  lcd.print("C");
  delay(2000);
}
