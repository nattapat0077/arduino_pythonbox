#include "DHT.h"
#define DHTPIN D6
#define DHTTYPE DHT11 //DHT 11
#include <TridentTD_LineNotify.h>

#define SSID "Nook" //wifi ssid 
#define PASSWORD "nook247538" //wifi password
#define LINE_TOKEN "EoEsFlWHogGKL0gXo86NTSGHiOpUywVaDZx8H4SoRNH" // Line Token

DHT dht(DHTPIN, DHTTYPE);
int analogPin = A0; //define analogpin
int analog_val = 0;

void setup() {

    Serial.begin(9600);
    dht.begin();

    Serial.println();
    Serial.println(LINE.getVersion());
    WiFi.begin(SSID, PASSWORD);
    Serial.printf("WiFi connecting to %s\n", SSID);

    while (WiFi.status() != WL_CONNECTED) {
        Serial.print(".");
        delay(2000);
    }

    Serial.printf("\nWiFi connected\nIP : ");
    Serial.println(WiFi.localIP());
    LINE.setToken(LINE_TOKEN);
    LINE.notify("เซนเซอร์วัดอุณหภูมิ ความชื้นและเซนเซอร์ตรวจจับน้ำหกหรืองูปัสสาวะ เริ่มทำงานแล้ว");
}

void loop() {
    String dht11_val = "";
    float h = dht.readHumidity(); //Humid
    float t = dht.readTemperature(); //Temp
    
    analog_val = analogRead(analogPin);
    Serial.print("analog val = "); //send msg to computer "val = "
    Serial.println(analog_val); //print val

    if (isnan(h) || isnan(t)) {
        Serial.println("Failed to read from DHT sensor!");
        return;
    } else {
        dht11_val = dht11_val + "ขณะนี้อุณหภูมิ "+t+"°C"+ " ความชื้น "+h+"%";

        Serial.println(dht11_val);
        LINE.notify(dht11_val);
        
    }

    if (analog_val < 1024) {
        LINE.notify("ตอนนี้มีน้ำหกหรืองูปัสสาวะนะจ๊ะ");
    } else {
        Serial.println(analog_val);
        
    }
}